'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Boolean n condition test', () => {

	it('tests NOT condition with value of true', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: true }));

		const factoryCondition = new FactoryCondition();

		const condition = factoryCondition.createCondition('NOT', parameters);

		assert.strictEqual(condition.verify(), false);
	});

	it('tests NOT condition with value of true', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: false }));
		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('NOT', parameters);

		assert.strictEqual(condition.verify(), true);
	});

});
