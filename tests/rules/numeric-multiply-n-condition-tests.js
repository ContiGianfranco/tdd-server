'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Numeric Multiply n condition test', () => {

	it('Multiply of one element returns the element', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('*', parameters);

		assert.strictEqual(condition.verify(), 5);
	});

	it('Multiply of one 5 and 1 returns 5', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));
		parameters.push(new ConstantArgument({ value: 1 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('*', parameters);

		assert.strictEqual(condition.verify(), 5);
	});

	it('Multiply of one -1 and 5 returns 0', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));
		parameters.push(new ConstantArgument({ value: -1 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('*', parameters);

		assert.strictEqual(condition.verify(), -5);
	});

	it('Multiply of 5 and 0 returns 15', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));
		parameters.push(new ConstantArgument({ value: 0 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('*', parameters);

		assert.strictEqual(condition.verify(), 0);
	});

	it('Multiply of 1,2,3,4,5 returns 120', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 2 }));
		parameters.push(new ConstantArgument({ value: 3 }));
		parameters.push(new ConstantArgument({ value: 4 }));
		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('*', parameters);

		assert.strictEqual(condition.verify(), 120);
	});

});
