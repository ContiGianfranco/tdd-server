'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Rule Inequality test', () => {
	it('testing Less Than Condition with two values of 7 and 9', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 7 }));

		parameters.push(new ConstantArgument({ value: 9 }));

		parameters.push(new ConstantArgument({ value: 12 }));

		parameters.push(new ConstantArgument({ value: 16 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('<', parameters);

		assert.strictEqual(condition.verify(), true);


	});

	it('testing Less or Equal Condition with < two values of 9 and 9', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 9 }));

		parameters.push(new ConstantArgument({ value: 9 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('<=', parameters);

		assert.strictEqual(condition.verify(), true);


	});

	it('testing Greater Condition with < two values of 18 and 12', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 18 }));

		parameters.push(new ConstantArgument({ value: 12 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('>', parameters);

		assert.strictEqual(condition.verify(), true);


	});

	it('testing Less or Equal Condition with < two values of 11 and 11', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 11 }));

		parameters.push(new ConstantArgument({ value: 11 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('>=', parameters);

		assert.strictEqual(condition.verify(), true);


	});
});
