'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Boolean n condition test', () => {

	it('tests - condition with two values of 9 and 7', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 9 }));

		parameters.push(new ConstantArgument({ value: 7 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('-', parameters);

		assert.strictEqual(condition.verify(), 2);
	});

	it('tests / condition with two values of 4 and 2', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 4 }));

		parameters.push(new ConstantArgument({ value: 2 }));
		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('/', parameters);

		assert.strictEqual(condition.verify(), 2);
	});

	it('tests / condition with two values of 4 and a - condition between 8 and 4', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 8 }));

		parameters.push(new ConstantArgument({ value: 4 }));

		const factoryCondition = new FactoryCondition();

		const subCondition = factoryCondition.createCondition('-', parameters);
		const divParameters = [new ConstantArgument({ value: 4 }), subCondition];
		const divCondition = factoryCondition.createCondition('/', divParameters);

		parameters.push(new ConstantArgument({ value: 2 }));

		assert.strictEqual(divCondition.verify(), 1);
	});
});
