'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('First condition test', () => {
	it('tests obtaining the first element of an array', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 12 }));

		parameters.push(new ConstantArgument({ value: 8 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('FIRST', parameters);

		assert.strictEqual(condition.verify(), 12);
	});
});
