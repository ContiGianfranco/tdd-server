'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Boolean n condition test', () => {

	it('tests NEGATE condition with value of 5', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('NEGATE', parameters);

		assert.strictEqual(condition.verify(), -5);
	});

	it('tests NEGATE condition with value of -7', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: -7 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('NEGATE', parameters);

		assert.strictEqual(condition.verify(), 7);
	});

});
