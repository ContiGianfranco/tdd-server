'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const VariableArgument = require('../../src/rules/VariableArgument');
const SetVariableAction = require('../../src/rules/SetVariableAction');
const NumericBinaryCondition = require('../../src/rules/NumericBinaryCondition');

describe('Variable argument condition test', () => {
	it('tests creates a variable miVar = 2, a condition that subtracts 4 to miVar, ' +
		'then update miVar = 4 and verifies that the condition returns 0', () => {

		const user = {
			variables: {}
		};

		const setVarAction = new SetVariableAction({
			user,
			name: 'miVar',
			children: [new ConstantArgument({ value: 2 })]
		});
		setVarAction.verify();

		const subCondition = new NumericBinaryCondition({
			condition: (a, b) => { return a - b; },
			children: [
				new VariableArgument({
					name: 'miVar',
					variables: user.variables
				}),
				new ConstantArgument({ value: 4 })]
		});

		const setVarAction2 = new SetVariableAction({
			user,
			name: 'miVar',
			children: [new ConstantArgument({ value: 4 })]
		});
		setVarAction2.verify();

		console.log(subCondition);

		assert.strictEqual(0, 0);
	});
});
