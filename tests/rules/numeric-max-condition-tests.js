'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Numeric Max condition test', () => {

	it('Max between 1 and 0 returns 1', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 0 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('MAX', parameters);

		assert.strictEqual(condition.verify(), 1);
	});

	it('Max between 5 and -5 returns 5', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));
		parameters.push(new ConstantArgument({ value: -5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('MAX', parameters);

		assert.strictEqual(condition.verify(), 5);
	});

	it('Max of 1,2,3,4,5 returns 5', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: -1 }));
		parameters.push(new ConstantArgument({ value: 2 }));
		parameters.push(new ConstantArgument({ value: 3 }));
		parameters.push(new ConstantArgument({ value: 4 }));
		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('MAX', parameters);

		assert.strictEqual(condition.verify(), 5);
	});

	it('Max of 20,30,40,50 returns 50', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 20 }));
		parameters.push(new ConstantArgument({ value: 30 }));
		parameters.push(new ConstantArgument({ value: 40 }));
		parameters.push(new ConstantArgument({ value: 50 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('MAX', parameters);

		assert.strictEqual(condition.verify(), 50);
	});

});
