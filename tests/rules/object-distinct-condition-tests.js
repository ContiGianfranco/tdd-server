'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const ObjectNCondition = require('../../src/rules/ObjectNCondition');
const ObjectDistinctFunction = require('../../src/rules/ObjectDistinctFunction');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Object Distinct condition test', () => {

	it('Distinct with 1 element returns true', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('DISTINCT', parameters);

		assert.strictEqual(condition.verify(), true);
	});

	it('Distinct with 5 and 5 returns false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));
		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('DISTINCT', parameters);

		assert.strictEqual(condition.verify(), false);
	});

	it('Distinct of 1,2,3,4,5 returns false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: -1 }));
		parameters.push(new ConstantArgument({ value: 2 }));
		parameters.push(new ConstantArgument({ value: 3 }));
		parameters.push(new ConstantArgument({ value: 4 }));
		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('DISTINCT', parameters);

		assert.strictEqual(condition.verify(), true);
	});

	it('Distinct of 1,1,1,2 returns false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 2 }));

		const condition = new ObjectNCondition({
			function: new ObjectDistinctFunction(),
			children: parameters
		});

		assert.strictEqual(condition.verify(), false);
	});

	it('Distinct of 1,1,2,1 returns false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 2 }));
		parameters.push(new ConstantArgument({ value: 1 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('DISTINCT', parameters);

		assert.strictEqual(condition.verify(), false);
	});

	it('Distinct of hi and hi returns false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 'hi' }));
		parameters.push(new ConstantArgument({ value: 'hi' }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('DISTINCT', parameters);

		assert.strictEqual(condition.verify(), false);
	});

	it('Distinct of hi and bye returns true', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 'hi' }));
		parameters.push(new ConstantArgument({ value: 'bye' }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('DISTINCT', parameters);

		assert.strictEqual(condition.verify(), true);
	});

});
