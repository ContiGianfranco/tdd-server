'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Numeric Average condition test', () => {

	it('Average of one element returns the element', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('AVERAGE', parameters);

		assert.strictEqual(condition.verify(), 5);
	});

	it('Average of one 5 and 1 returns 3', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));
		parameters.push(new ConstantArgument({ value: 1 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('AVERAGE', parameters);

		assert.strictEqual(condition.verify(), 3);
	});

	it('Average of one -5 and 5 returns 0', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));
		parameters.push(new ConstantArgument({ value: -5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('AVERAGE', parameters);

		assert.strictEqual(condition.verify(), 0);
	});

	it('Average of 1,2,3,4,5 returns 3', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 2 }));
		parameters.push(new ConstantArgument({ value: 3 }));
		parameters.push(new ConstantArgument({ value: 4 }));
		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('AVERAGE', parameters);

		assert.strictEqual(condition.verify(), 3);
	});

});
