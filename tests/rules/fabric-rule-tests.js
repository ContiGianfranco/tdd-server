'use strict';

const assert = require('assert');

const FactoryRule = require('../../src/Factory/FactoryRule');

describe('Factory Rule Tests', () => {

	it('tests - condition with two values of 1 and 2 are not equal so action should not be execute', () => {

		const jsonRule = {
			name: 'Comprar algo',
			condition: {
				type: 'CALL',
				name: '==',
				arguments: [
					{
						type: 'CONSTANT',
						value: 1
					},
					{
						type: 'CONSTANT',
						value: 2
					}]
			},
			action: [{
				type: 'SELL_MARKET',
				symbol: 'BTC/USDT',
				amount: 1
			}]
		};
		const factoryRule = new FactoryRule();
		const rule = factoryRule.createRule(jsonRule);
		assert.notStrictEqual(rule.check(), 1);


	});

	it('tests - condition with two values of 1 and 1 are equal so action should be execute', () => {

		const jsonRule = {
			name: 'Comprar algo',
			condition: {
				type: 'CALL',
				name: '==',
				arguments: [
					{
						type: 'CONSTANT',
						value: 1
					},
					{
						type: 'CONSTANT',
						value: 1
					}]
			},
			action: [{
				type: 'SELL_MARKET',
				symbol: 'BTC/USDT',
				amount: {
					type: 'CONSTANT',
					value: 0.005
				}
			}]
		};
		const factoryRule = new FactoryRule();
		const rule = factoryRule.createRule(jsonRule);
		rule.check().then(value => assert.strictEqual(value, 0.005));
	});

	it('tests - composed condition with two values of 1 and 1 are equal so action should be execute', () => {

		const jsonRule = {
			name: 'Comprar algo',
			condition: {
				type: 'CALL',
				name: '==',
				arguments: [
					{
						type: 'CALL',
						name: '*',
						arguments: [
							{
								type: 'CONSTANT',
								value: 1
							},
							{
								type: 'CONSTANT',
								value: 1
							}
						]
					},
					{
						type: 'CONSTANT',
						value: 1
					}]
			},
			action: [{
				type: 'BUY_MARKET',
				symbol: 'BTC/USDT',
				amount: {
					type: 'CONSTANT',
					value: 1
				}
			}]
		};
		const factoryRule = new FactoryRule();
		const rule = factoryRule.createRule(jsonRule);
		rule.check().then(value => assert.strictEqual(value, 0.005));
	});

	it('tests - bla bla', () => {

		const jsonRule = {
			name: 'Escape',
			condition: {
				type: 'CALL',
				name: '<',
				arguments: [{
					type: 'CALL',
					name: 'LAST',
					arguments: [{
						type: 'DATA',
						symbol: 'BTC/USDT',
						since: 3600,
						until: 0,
						default: {
							type: 'VARIABLE',
							name: 'LIMIT_VALUE_BTC/USDT'
						}
					}]
				}, {
					type: 'VARIABLE',
					name: 'LIMIT_VALUE_BTC/USDT'
				}]
			},
			action: [{
				type: 'SELL_MARKET',
				symbol: 'BTC/USDT',
				amount: {
					type: 'WALLET',
					symbol: 'BTC'
				}
			}],
			_userID: '15263456'
		};

		const factoryRule = new FactoryRule();
		const rule = factoryRule.createRule(jsonRule);
		rule.check().then(value => assert.strictEqual(value, 39680.29));
	});
});
