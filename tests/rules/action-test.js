'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const SetVariableAction = require('../../src/rules/SetVariableAction');
const NumericBinaryCondition = require('../../src/rules/NumericBinaryCondition');

describe('Actions condition test', () => {
	it('tests setting a new variable MarkerState as Stable for user', async () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 'Stable' }));

		const user = {
			userID: '123456'
		};

		const setVarAction = new SetVariableAction({
			user,
			name: 'MarkerState',
			children: parameters
		});


		assert.strictEqual(await setVarAction.verify(user), 'Stable');
	});

	it('tests updating a variable division as 8/4', async () => {

		const user = {
			userID: '123456'
		};

		const setVarAction = new SetVariableAction({
			user,
			name: 'division',
			children: [new NumericBinaryCondition({
				condition: (a, b) => {
					return a / b;
				},
				children: [new ConstantArgument({ value: 8 }), new ConstantArgument({ value: 4 })]
			})]
		});


		assert.strictEqual(await setVarAction.verify(user), 2);
	});
});
