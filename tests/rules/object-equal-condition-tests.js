'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Object Equal condition test', () => {

	it('Equal with 1 element returns true', async () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));
		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('==', parameters);

		assert.strictEqual(await condition.verify('12345'), true);
	});

	it('Equal with 5 and 5 returns true', async () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 5 }));
		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('==', parameters);


		assert.strictEqual(await condition.verify('12345'), true);
	});

	it('Equal of 1,2,3,4,5 returns false', async () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: -1 }));
		parameters.push(new ConstantArgument({ value: 2 }));
		parameters.push(new ConstantArgument({ value: 3 }));
		parameters.push(new ConstantArgument({ value: 4 }));
		parameters.push(new ConstantArgument({ value: 5 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('==', parameters);


		assert.strictEqual(await condition.verify('12345'), false);
	});

	it('Equal of 1,1,1,2 returns false', async () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 2 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('==', parameters);

		assert.strictEqual(await condition.verify('12345'), false);
	});

	it('Equal of 1,1,2,1 returns false', async () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 1 }));
		parameters.push(new ConstantArgument({ value: 2 }));
		parameters.push(new ConstantArgument({ value: 1 }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('==', parameters);


		assert.strictEqual(await condition.verify('12345'), false);
	});

	it('Equal of hi and hi returns true', async () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 'hi' }));
		parameters.push(new ConstantArgument({ value: 'hi' }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('==', parameters);

		assert.strictEqual(await condition.verify('12345'), true);
	});

	it('Equal of hi and bye returns false', async () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 'hi' }));
		parameters.push(new ConstantArgument({ value: 'bye' }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('==', parameters);

		assert.strictEqual(await condition.verify('12345'), false);
	});

});
