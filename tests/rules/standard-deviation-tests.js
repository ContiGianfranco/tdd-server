'use strict';

const chai = require('chai');

const { expect } = chai;
const StandardDeviationCondition = require('../../src/rules/StandardDeviationCondition');
const ConstantArgument = require('../../src/rules/ConstantArgument');

describe('Rule Standard Deviation test', () => {
	it('testing Standard Deviation with four values', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 7 }));

		parameters.push(new ConstantArgument({ value: 9 }));

		parameters.push(new ConstantArgument({ value: 12 }));

		parameters.push(new ConstantArgument({ value: 16 }));

		const condition = new StandardDeviationCondition({
			children: parameters
		});

		expect(condition.verify()).to.be.closeTo(3.3911, 1e-4);


	});

	it('testing Standard Deviation with four values floating point and see if it returns the value expected', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: 15.35 }));

		parameters.push(new ConstantArgument({ value: 23.42 }));

		parameters.push(new ConstantArgument({ value: 61.20 }));

		parameters.push(new ConstantArgument({ value: 125 }));

		const condition = new StandardDeviationCondition({
			children: parameters
		});

		expect(condition.verify()).to.be.closeTo(43.3061, 1e-4);

	});
});
