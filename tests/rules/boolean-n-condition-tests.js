'use strict';

const assert = require('assert');
const ConstantArgument = require('../../src/rules/ConstantArgument');
const BooleanNCondition = require('../../src/rules/BooleanNCondition');
const FactoryCondition = require('../../src/Factory/FactoryCondition');

describe('Numeric Binary condition test', () => {
	it('tests AND condition with two values of true and true', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: true }));

		parameters.push(new ConstantArgument({ value: true }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('AND', parameters);

		assert.strictEqual(condition.verify(), true);
	});

	it('tests AND condition with two values of true and false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: true }));

		parameters.push(new ConstantArgument({ value: false }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('AND', parameters);

		assert.strictEqual(condition.verify(), false);
	});

	it('tests AND condition with two values of false and false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: false }));

		parameters.push(new ConstantArgument({ value: false }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('AND', parameters);

		assert.strictEqual(condition.verify(), false);
	});

	it('tests AND condition with tree values of false, true, and true', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: false }));

		parameters.push(new ConstantArgument({ value: true }));

		parameters.push(new ConstantArgument({ value: true }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('AND', parameters);

		assert.strictEqual(condition.verify(), false);
	});

	it('tests OR condition with two values of true and true', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: true }));

		parameters.push(new ConstantArgument({ value: true }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('OR', parameters);

		assert.strictEqual(condition.verify(), true);
	});

	it('tests OR condition with two values of true and false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: true }));

		parameters.push(new ConstantArgument({ value: false }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('OR', parameters);

		assert.strictEqual(condition.verify(), true);
	});

	it('tests OR condition with two values of false and false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: false }));

		parameters.push(new ConstantArgument({ value: false }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('OR', parameters);

		assert.strictEqual(condition.verify(), false);
	});

	it('tests OR condition with tree values of false, false, and true', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: false }));

		parameters.push(new ConstantArgument({ value: false }));

		parameters.push(new ConstantArgument({ value: true }));

		const condition = new BooleanNCondition({
			condition: (a, b) => { return a || b; },
			children: parameters
		});

		assert.strictEqual(condition.verify(), true);
	});

	it('tests OR condition with tree values of true, false, and false', () => {

		const parameters = [];

		parameters.push(new ConstantArgument({ value: true }));

		parameters.push(new ConstantArgument({ value: false }));

		parameters.push(new ConstantArgument({ value: false }));

		const factoryCondition = new FactoryCondition();
		const condition = factoryCondition.createCondition('OR', parameters);

		assert.strictEqual(condition.verify(), true);
	});
});
