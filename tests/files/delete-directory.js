
'use strict';

const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const sandbox = require('sinon').createSandbox();

const { TokenServices } = require('../../src/services/index');
const Model = require('../../src/databases/mongodb/model');
const STATUS_CODES = require('../../src/utils/status-codes.json');

process.env.PORT = 3030;

const app = require('../../src/index');

chai.use(chaiHttp);

describe('Files', async () => {

	beforeEach(() => {
		process.env.PORT = 3030;
		sandbox.stub(TokenServices.prototype, 'verifyToken').resolves({ _id: '60496180a0e2305d7103dc0c' });
	});

	afterEach(() => {
		delete process.env.PORT;
		sandbox.restore();
	});

	const fakeFile = {
		_id: '60465d25f28dcf001ff6dd6f',
		type: 'file',
		name: 'foto.png',
		url: 'https://firebasestorage.googleapis.com/v0/b/tddrive-b11e3.appspot.com/o/foto.png?alt=media&token=0942021d-e77a-478f-bad2-24b0596ba864',
		path: '60496180a0e2305d7103dc0c/myDocuments',
		user: '60456ebb0190bf001f6bbee2',
		createdAt: '2021-03-08T17:21:41.686Z',
		updatedAt: '2021-03-08T17:21:41.686Z',
		canShare: true,
		__v: 0
	};

	describe('Delete Directory', async () => {

		it('Should set status code 200 when it can create the directory (no children)', async () => {

			sandbox.stub(Model.prototype, 'remove').resolves(fakeFile);
			sandbox.stub(Model.prototype, 'findBy').resolves([]);

			const res = await chai.request(app).delete('/api/files/directory/?path=myDocuments/')
				.set('x-auth-token', 'some-valid-token');

			assert.deepStrictEqual(res.status, STATUS_CODES.OK);

			sandbox.assert.calledOnce(Model.prototype.remove);
			sandbox.assert.calledOnce(Model.prototype.findBy);
		});

		it('Should set status code 400 when the directory have children', async () => {

			sandbox.stub(Model.prototype, 'remove').resolves(fakeFile);
			sandbox.stub(Model.prototype, 'findBy').resolves([fakeFile]);

			const res = await chai.request(app).delete('/api/files/directory/?path=myDocuments/')
				.set('x-auth-token', 'some-valid-token');

			assert.deepStrictEqual(res.status, STATUS_CODES.BAD_REQUEST);

			sandbox.assert.notCalled(Model.prototype.remove);
			sandbox.assert.calledOnce(Model.prototype.findBy);
		});


		it('Should set status code 404 when it can not delete de file', async () => {

			sandbox.stub(Model.prototype, 'remove').resolves(null);
			sandbox.stub(Model.prototype, 'findBy').resolves([]);

			const res = await chai.request(app).delete('/api/files/directory/?path=myDocuments/')
				.set('x-auth-token', 'some-valid-token');

			assert.deepStrictEqual(res.status, STATUS_CODES.NOT_FOUND);

			sandbox.assert.calledOnce(Model.prototype.remove);
			sandbox.assert.calledOnce(Model.prototype.findBy);
		});


		it('Should set status code 500 when database fails', async () => {

			sandbox.stub(Model.prototype, 'findBy').rejects(new Error('DB ERROR'));

			const res = await chai.request(app).delete('/api/files/directory/?path=myDocuments/')
				.set('x-auth-token', 'some-valid-token');

			assert.deepStrictEqual(res.status, STATUS_CODES.INTERNAL_SERVER_ERROR);
			assert.deepStrictEqual(res.body, { message: 'DB ERROR' });

			sandbox.assert.calledOnce(Model.prototype.findBy);
		});
	});
});
