
'use strict';

const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const sandbox = require('sinon').createSandbox();

const { TokenServices } = require('../../src/services/index');
const Model = require('../../src/databases/mongodb/model');
const STATUS_CODES = require('../../src/utils/status-codes.json');

process.env.PORT = 3030;

const app = require('../../src/index');

chai.use(chaiHttp);

describe('Files', async () => {

	beforeEach(() => {
		process.env.PORT = 3030;
		sandbox.stub(TokenServices.prototype, 'verifyToken').resolves({ _id: '60496180a0e2305d7103dc0c' });
	});

	afterEach(() => {
		delete process.env.PORT;
		sandbox.restore();
	});

	const fakeFile = {
		_id: '60465d25f28dcf001ff6dd6f',
		type: 'file',
		name: 'foto.png',
		url: 'https://firebasestorage.googleapis.com/v0/b/tddrive-b11e3.appspot.com/o/foto.png?alt=media&token=0942021d-e77a-478f-bad2-24b0596ba864',
		path: '60496180a0e2305d7103dc0c/myDocuments',
		user: '60456ebb0190bf001f6bbee2',
		createdAt: '2021-03-08T17:21:41.686Z',
		updatedAt: '2021-03-08T17:21:41.686Z',
		canShare: true,
		__v: 0
	};

	const fakeUser = {
		_id: '60456ebb0190bf001f6bbee2',
		canShare: ['60465d25f28dcf001ff6dd6f'],
		name: 'Ariel',
		surname: 'Piro Martino',
		birthDate: '1996-08-03T00:00:00.000Z',
		email: 'ariel.piro@hotmail.com',
		password: '$2b$10$FVLh9oI6betv13edzE9cQuNbXVFqTu3pp3MfKP9mp9Uv/rVXQuDf6',
		provider: 'email',
		profilePic: 'https://firebasestorage.googleapis.com/v0/b/tddrive-b11e3.appspot.com/o/8a909882b3c6a2fcba6e1d4a42dabd42.jpg',
		createdAt: '2021-03-08T00:24:27.083Z',
		updatedAt: '2021-03-09T21:43:10.726Z',
		__v: 0
	};

	describe('Get tree', async () => {

		it('Should return empty tree when no file is found', async () => {

			const findStub = sandbox.stub(Model.prototype, 'find');

			findStub.onCall(0).resolves([]);

			sandbox.stub(Model.prototype, 'findBy').resolves([fakeUser]);
			findStub.onCall(1).resolves([]);
			findStub.onCall(2).resolves([]);

			const res = await chai.request(app).get('/api/files/tree')
				.set('x-auth-token', 'some-valid-token');

			assert.deepStrictEqual(res.status, STATUS_CODES.OK);

			sandbox.assert.callCount(Model.prototype.find, 3);
		});


		it('Should return empty tree when no file is found', async () => {

			const findStub = sandbox.stub(Model.prototype, 'find');

			findStub.onCall(0).resolves([fakeFile]);

			sandbox.stub(Model.prototype, 'findBy').resolves([fakeUser]);
			findStub.onCall(1).resolves([]);
			findStub.onCall(2).resolves([]);

			const res = await chai.request(app).get('/api/files/tree')
				.set('x-auth-token', 'some-valid-token');

			assert.deepStrictEqual(res.status, STATUS_CODES.OK);
			assert.deepStrictEqual(res.body, [{
				id: 'root',
				name: '/',
				path: '/',
				children: [{ id: '60465d25f28dcf001ff6dd6f', name: 'myDocumentsfoto.png', children: [], path: '/myDocuments' }]
			}
			]);

			sandbox.assert.callCount(Model.prototype.find, 3);
		});

		it('Should set status code 500 when database fails', async () => {

			sandbox.stub(Model.prototype, 'find').rejects(new Error('DB ERROR'));

			const res = await chai.request(app).get('/api/files/tree')
				.set('x-auth-token', 'some-valid-token');
			assert.deepStrictEqual(res.status, STATUS_CODES.INTERNAL_SERVER_ERROR);
			assert.deepStrictEqual(res.body, { message: 'DB ERROR' });

			sandbox.assert.calledOnce(Model.prototype.find);
		});
	});
});
