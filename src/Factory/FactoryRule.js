'use strict';

const Rule = require('../rules/Rule');
const ConstantArgument = require('../rules/ConstantArgument');
const FactoryCondition = require('./FactoryCondition');
const FactoryAction = require('./FactoryAction');
const DataArgument = require('../rules/DataArgument');
const WalletArgument = require('../rules/WalletArgument');
const VariableArgument = require('../rules/VariableArgument');

class FactoryRule {
	createRule(jsonRule) {
		const actions = [];
		const ruleName = jsonRule.name;
		const userID = jsonRule._userID;
		const ruleAction = [];
		const ruleCondition = this.createCondition(jsonRule.condition, userID);
		// eslint-disable-next-line guard-for-in,no-restricted-syntax
		for(const action of jsonRule.action)
			actions.push(this.createAction(action, userID));
		ruleAction.push(actions[0]);
		return new Rule(ruleName, ruleAction, ruleCondition, userID);
	}

	createCondition(jsonCondition, userID) {
		const parameters = [];
		if(jsonCondition.type === 'CONSTANT')
			return new ConstantArgument({ value: jsonCondition.value });

		for(const argument of jsonCondition.arguments) {
			switch(argument.type) {
				case 'CALL':
					parameters.push(this.createCondition(argument, userID));
					break;
				case 'CONSTANT':
					parameters.push(new ConstantArgument({ value: argument.value }));
					break;
				case 'WALLET':
					parameters.push(new WalletArgument({
						asset: argument.symbol
					}));
					break;
				case 'VARIABLE':
					parameters.push(new VariableArgument({ userID, name: argument.name }));
					break;
				case 'DATA':
					parameters.push(new DataArgument({ symbol: argument.symbol, from: argument.from, until: argument.until }));
					break;
				default:
					break;
			}
		}
		const factoryCondition = new FactoryCondition();
		return factoryCondition.createCondition(jsonCondition.name, parameters);
	}

	createAction(jsonAction, userID) {
		const factoryAction = new FactoryAction();
		return factoryAction.createAction(jsonAction, userID);
	}

}

module.exports = FactoryRule;
