'use strict';

const BooleanNotCondition = require('../rules/BooleanNotCondition');
const BooleanNCondition = require('../rules/BooleanNCondition');
const FirstCondition = require('../rules/FirstCondition');
const LastCondition = require('../rules/LastCondition');
const InequalityCondition = require('../rules/MathInequalityCondition');
const { LessThan, 	LessThanEqual,	GreaterThan,	GreaterThanEqual } = require('../rules/MathInequalityCondition');
const ObjectNCondition = require('../rules/ObjectNCondition');
const ObjectEqualFunction = require('../rules/ObjectEqualFunction');
const ObjectDistinctFunction = require('../rules/ObjectDistinctFunction');
const NumericNegateCondition = require('../rules/NumericNegateCondition');
const NumericBinaryCondition = require('../rules/NumericBinaryCondition');
const NumericNCondition = require('../rules/NumericNCondition');
const StandardDeviationCondition = require('../rules/StandardDeviationCondition');

class FactoryCondition {

	createCondition(type, parameters) {
		switch(type) {
			case 'NOT':
				return new BooleanNotCondition({ children: parameters });

			case 'AND':
				return new BooleanNCondition({
					condition: (a, b) => { return a && b; },
					children: parameters
				});

			case 'OR':
				return new BooleanNCondition({
					condition: (a, b) => { return a || b; },
					children: parameters
				});

			case 'FIRST':
				return new FirstCondition({
					children: parameters
				});

			case 'LAST':
				return new LastCondition({
					children: parameters
				});

			case '<':
				return new InequalityCondition.MathInequalityCondition({
					condition: LessThan,
					children: parameters
				});

			case '<=':
				return new InequalityCondition.MathInequalityCondition({
					condition: LessThanEqual,
					children: parameters
				});

			case '>':
				return new InequalityCondition.MathInequalityCondition({
					condition: GreaterThan,
					children: parameters
				});

			case '>=':
				return new InequalityCondition.MathInequalityCondition({
					condition: GreaterThanEqual,
					children: parameters
				});

			case '==':
				return new ObjectNCondition({
					function: new ObjectEqualFunction(),
					children: parameters
				});

			case 'NEGATE':
				return new NumericNegateCondition({
					children: parameters
				});

			case '-':
				return new NumericBinaryCondition({
					condition: (a, b) => { return a - b; },
					children: parameters
				});

			case '/':
				return new NumericBinaryCondition({
					condition: (a, b) => { return a / b; },
					children: parameters
				});

			case 'DISTINCT':
				return new ObjectNCondition({
					function: new ObjectDistinctFunction(),
					children: parameters
				});

			case 'AVERAGE':
				return new NumericNCondition({
					condition: (a, b) => { return a + b; },
					initial: 0,
					div: parameters.length,
					children: parameters
				});
			case '+':
				return new NumericNCondition({
					condition: (a, b) => { return a + b; },
					initial: 0,
					div: 1,
					children: parameters
				});

			case '*':
				return new NumericNCondition({
					condition: (a, b) => { return a * b; },
					initial: 1,
					div: 1,
					children: parameters
				});

			case 'MAX':
				return new NumericNCondition({
					condition: (a, b) => {
						if(a < b)
							return b;
						return a;
					},
					initial: parameters[0].verify(),
					div: 1,
					children: parameters
				});

			case 'MIN':
				return new NumericNCondition({
					condition: (a, b) => {
						if(b < a)
							return b;
						return a;
					},
					initial: parameters[0].verify(),
					div: 1,
					children: parameters
				});

			case 'STDDEV':
				return new StandardDeviationCondition({
					children: parameters
				});

			default:
				break;
		}
	}
}

module.exports = FactoryCondition;
