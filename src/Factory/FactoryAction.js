'use strict';

const SellAction = require('../rules/SellAction');
const BuyAction = require('../rules/BuyAction');
const ConstantArgument = require('../rules/ConstantArgument');
const WalletArgument = require('../rules/WalletArgument');
const SetVariableAction = require('../rules/SetVariableAction');
const FactoryCondition = require('./FactoryCondition');
const VariableArgument = require('../rules/VariableArgument');
const DataArgument = require('../rules/DataArgument');


class FactoryAction {

	createAction(parameters, userID) {
		let child;
		if(parameters.symbol)
			parameters.symbol = parameters.symbol.replace('/', '');
		switch(parameters.type) {
			case 'SELL_MARKET':
				switch(parameters.amount.type) {
					case 'CONSTANT':
						child = new ConstantArgument({ value: parameters.amount.value });
						break;
					case 'WALLET':
						child = new WalletArgument({
							asset: parameters.amount.symbol
						});
						break;
					case 'VARIABLE':
						child = new VariableArgument({ userID, name: parameters.value.name });
						break;
					default:
						break;
				}
				return new SellAction({ symbol: parameters.symbol, children: [child] });

			case 'BUY_MARKET':
				switch(parameters.amount.type) {
					case 'CONSTANT':
						child = new ConstantArgument({ value: parameters.amount.value });
						break;
					case 'WALLET':
						child = new WalletArgument({
							asset: parameters.amount.symbol
						});
						break;
					case 'VARIABLE':
						child = new VariableArgument({ userID, name: parameters.value.name });
						break;
					default:
						break;
				}
				return new BuyAction({ symbol: parameters.symbol, children: [child] });
			case 'SET_VARIABLE':
				switch(parameters.value.type) {
					case 'CONSTANT':
						child = new ConstantArgument({ value: parameters.amount.value });
						break;
					case 'WALLET':
						child = new WalletArgument({
							asset: parameters.amount.symbol
						});
						break;
					case 'VARIABLE':
						child = new VariableArgument({ name: parameters.value.name });
						break;
					case 'CALL':
						child = this.createVariableCondition(parameters.value);
						break;
					default:
						break;
				}
				return new SetVariableAction({ userID, name: parameters.name, children: [child] });
			default:
				break;
		}
	}

	createVariableCondition(jsonCondition) {
		const parameters = [];
		// eslint-disable-next-line guard-for-in,no-restricted-syntax
		for(const argument of jsonCondition.arguments) {
			switch(argument.type) {
				case 'CALL':
					parameters.push(this.createVariableCondition(argument));
					break;
				case 'CONSTANT':
					parameters.push(new ConstantArgument({ value: argument.value }));
					break;
				case 'WALLET':
					parameters.push(new WalletArgument({
						asset: argument.symbol
					}));
					break;
				case 'VARIABLE':
					parameters.push(new VariableArgument({ name: argument.value.name }));
					break;
				case 'DATA':
					parameters.push(new DataArgument({ symbol: argument.symbol, from: argument.from, until: argument.until }));
					break;
				default:
					break;
			}
		}
		const factoryCondition = new FactoryCondition();
		return factoryCondition.createCondition(jsonCondition.name, parameters);
	}
}

module.exports = FactoryAction;
