'use strict';


const CryptoJS = require('crypto-js');
const chai = require('chai');

const url = 'https://testnet.binance.vision/';
const route = 'api/v3/account';

const getWallet = async (req, res) => {
	console.log('POST/api/wallet/');
	const { privatekey, publickey } = req.headers;
	const now = new Date();
	const timestamp = now.getTime();

	const signature = CryptoJS.HmacSHA256(
		`timestamp=${timestamp}`,
		privatekey
	).toString(CryptoJS.enc.Hex);

	const walletRes = await chai.request(url)
		.get(route)
		.set({
			'Content-Type': 'application/json',
			'X-MBX-APIKEY': publickey
		})
		.query({
			timestamp,
			signature
		});

	res.send(walletRes.body.balances);

};

module.exports = {
	getWallet
};
