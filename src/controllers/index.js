'use strict';

const users = require('./users');
const files = require('./files');
const rules = require('./rules');
const wallet = require('./wallet');

module.exports = {
	users,
	files,
	rules,
	wallet
};
