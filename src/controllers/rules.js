'use strict';

const STATUS_CODES = require('../utils/status-codes.json');
const Variables = require('../schemas/variables');
const Rules = require('../schemas/rules');
const Actions = require('../schemas/actions');
const Conditions = require('../schemas/conditions');
const Values = require('../schemas/values');
const Symbols = require('../schemas/symbols');
const Transaction = require('../schemas/transaction');
const { users } = require('../services/index');
const RuleManager = require('../rule-manager/rules-manager');
const KeysSchema = require('../schemas/keys');
const UserInfo = require('../services/user-info');
const Symbol = require('../schemas/symbols');

const addCondition = async (req, res) => {
	console.log('POST/api/rules/condition');
	const { userID } = req.body;
	const newCondition = new Conditions({ userID });
	res.json(newCondition);
};

const addAction = async (req, res) => {
	console.log('POST/api/rules/action');
	const { userID } = req.body;
	const data = new Actions({ userID });
	res.json(data);
};

const addValue = async (req, res) => {
	console.log('POST/api/rules/value');
	const { userID } = req.body;
	const data = new Values({ userID });
	res.json(data);
};

const addRule = async (req, res) => {
	console.log('POST/api/rules/rule');
	const { _userID, name, condition, action } = req.body;
	console.log(_userID);
	try {
		const user = await users.getUserById(_userID);
		if(!user)
			throw new Error(`It was not possible to add the rule to the user with ID: ${_userID}`);
		const newRule = new Rules({ _userID, name, condition, action });
		await Rules.create(newRule).then(rule => {
			res.status(STATUS_CODES.OK).send(rule);
		});
		await users.addNewRule(_userID, user, newRule);
		new RuleManager().update();
	} catch(err) {
		res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message: err.message });
	}
};

const addVariable = async (req, res) => {
	console.log('POST/api/rules/variable');
	const { userID, name, value } = req.body;
	// eslint-disable-next-line new-cap
	const data = new Variables({ userID, name, value });
	console.log(data);
	await Variables.create(data).then(val => {
		res.send(data);
		console.log(val);
	});
};

const addSymbol = async (req, res) => {
	console.log('POST/api/rules/symbol');
	const { userID } = req.body;
	const data = new Symbols({ userID });
	res.json(data);
};

const addKeys = async (req, res) => {

	try {
		const { userID, privateKey, publicKey } = req.body;
		const update = {
			userID,
			privateKey,
			publicKey
		};
		const options = { upsert: true };
		await KeysSchema.updateOne({ userID },	update, options).then(val => {
			console.log(val);
			res.status(STATUS_CODES.OK).send(update);
		});
		new UserInfo().update();
		res.status(STATUS_CODES.OK).send({ message: 'The keys were added' });

	} catch(err) {
		res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message: err.message });
	}
};

const getAllRules = async (req, res) => {
	const { userid } = req.headers;
	console.log(userid);
	const data = await Rules.find({ _userID: userid });
	res.json(data);
};

const getTransaction = async (req, res) => {
	console.log('GET/api/rules/transaction');
	const { userid } = req.headers;
	const data = await Transaction.find({ userID: userid });
	res.json(data);
};

const getKeys = async (req, res) => {
	try {
		const data = await KeysSchema.find({});
		res.json(data);
	} catch(e) {
		res.send({ message: e.message });
	}
};

const getVariables = async (req, res) => {
	try {
		const { userid } = req.headers;
		console.log(userid);
		const data = await Variables.find({ userID: userid });
		res.json(data);
	} catch(e) {
		res.send({ message: e.message });
	}
};

const getSymbolHistory = async (req, res) => {
	const data = await Symbol.find({});
	res.json(data);
};

const removeRuleFromUserById = async (req, res) => {

	const { userid, name } = req.headers;
	console.log(userid);
	try {
		const rule = await Rules.remove({ _userID: userid, name });
		await users.removeRules(userid);
		new RuleManager().update();
		console.log(rule);
		if(!rule)
			throw new Error(`It was not possible to remove the rule: ${req.body.name}`);
		res.status(STATUS_CODES.OK).send({ message: `Rules successfully removed from user with id: ${userid}` });
	} catch(err) {
		res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send({ message: err.message });
	}
};

module.exports = {
	addVariable,
	addCondition,
	addAction,
	addRule,
	addValue,
	addSymbol,
	addKeys,
	getAllRules,
	getTransaction,
	getKeys,
	getVariables,
	getSymbolHistory,
	removeRuleFromUserById
};
