
'use strict';

const mongoose = require('mongoose');

module.exports = () => {
	mongoose.connect('mongodb://mongo:27017/app', { // mongodb://mongo:27017/app
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
		autoIndex: true
	})
		.then(() => console.log('Connected to database'))
		.catch(e => console.log(e));
};
