'use strict';

class Action {
	constructor(type, symbol, amount) {
		this.type = type;
		this.symbol = symbol;
		this.amount = amount;
	}

	do() {
		return this.amount;
	}

}

module.exports = Action;
