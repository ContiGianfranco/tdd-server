'use strict';

const Component = require('./Component');

class LastCondition extends Component {

	async verify() {
		const val = await this.props.children[0].verify();
		return val.pop();
	}
}

module.exports = LastCondition;
