'use strict';

const Component = require('./Component');

class MathInequalityCondition extends Component {
	verify(userInfo) {
		let tmp = this.props.children[0].verify(userInfo);

		for(let i = 1; i < this.props.children.length; i++)
			tmp = this.props.condition(tmp, this.props.children[i].verify(userInfo));

		return tmp;
	}
}
const LessThan = (argument, userInfo) => {
	for(let i = 0; i < argument.length - 1; i++) {
		if(argument[i].verify(userInfo) >= argument[i + 1].verify(userInfo))
			return false;
	}
	return true;
};
const LessThanEqual = (argument, userInfo) => {
	for(let i = 0; i < argument.length - 1; i++) {
		if(argument[i].verify(userInfo) > argument[i + 1].verify(userInfo))
			return false;
	}
	return true;
};
const GreaterThan = (argument, userInfo) => {
	for(let i = 0; i < argument.length - 1; i++) {
		if(argument[i].verify(userInfo) <= argument[i + 1].verify(userInfo))
			return false;
	}
	return true;
};
const GreaterThanEqual = (argument, userInfo) => {
	for(let i = 0; i < argument.length - 1; i++) {
		if(argument[i].verify(userInfo) < argument[i + 1].verify(userInfo))
			return false;
	}
	return true;
};

module.exports = {
	MathInequalityCondition,
	LessThan,
	LessThanEqual,
	GreaterThan,
	GreaterThanEqual
};
