'use strict';

const Component = require('./Component');

class NumericNegateCondition extends Component {

	verify() {
		return -(this.props.children[0].verify());
	}
}

module.exports = NumericNegateCondition;
