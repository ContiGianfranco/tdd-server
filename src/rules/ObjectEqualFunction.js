'use strict';

class ObjectEqualFunction {
	async evaluate(parameters, userInfo) {
		let equal = true;
		for(let i = 0; i < parameters.length - 1; i++) {
			if(await parameters[i].verify(userInfo) !== await parameters[i + 1].verify(userInfo))
				equal = false;
		}
		return equal;
	}
}

module.exports = ObjectEqualFunction;
