'use strict';

class ObjectDistinctFunction {
	evaluate(parameters) {
		let equal = true;
		const counts = {};

		for(let i = 0; i < parameters.length; i++) {
			const num = parameters[i].verify();
			counts[num] = counts[num] ? counts[num] + 1 : 1;
		}

		// eslint-disable-next-line no-restricted-syntax
		for(const key in counts) {
			if(counts[key] > 1)
				equal = false;
		}
		return equal;
	}
}

module.exports = ObjectDistinctFunction;
