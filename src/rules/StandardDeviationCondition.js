'use strict';

const Component = require('./Component');

const Mid = argument => {
	let mid = 0;
	for(let i = 0; i < argument.length; i++)
		mid += argument[i].verify();
	return (mid / argument.length);
};

const StandardDeviation = (mid, argument) => {
	let distance = 0;
	for(let i = 0; i < argument.length; i++)
	// eslint-disable-next-line no-restricted-properties
		distance += Math.pow(argument[i].verify() - mid, 2);
	return (Math.sqrt(distance / argument.length));
};


class StandardDeviationCondition extends Component {
	verify() {
		const mid = Mid(this.props.children);
		const standardDeviation = StandardDeviation(mid, this.props.children);
		return standardDeviation;
	}
}


module.exports = StandardDeviationCondition;
