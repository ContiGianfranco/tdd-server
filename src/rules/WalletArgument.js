'use strict';

const CryptoJS = require('crypto-js');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const Component = require('./Component');

const url = 'https://testnet.binance.vision/';
const route = 'api/v3/account';

const getAssetWallet = (balances, asset) => {
	let i = 0;
	let found = false;
	let wallet;

	while(i < balances.length && !found) {
		if(balances[i].asset === asset) {
			found = true;
			wallet = balances[i];
		}
		i++;
	}
	return wallet;
};

class WalletArgument extends Component {
	async verify(userInfo) {
		const now = new Date();
		const timestamp = now.getTime();

		const signature = CryptoJS.HmacSHA256(
			`timestamp=${timestamp}`,
			userInfo.privateKey
		).toString(CryptoJS.enc.Hex);

		const res = await chai.request(url)
			.get(route)
			.set({
				'Content-Type': 'application/json',
				'X-MBX-APIKEY': userInfo.publicKey
			})
			.query({
				timestamp,
				signature
			});

		const wallet = getAssetWallet(res.body.balances, this.props.asset);

		return wallet.free;
	}
}

module.exports = WalletArgument;
