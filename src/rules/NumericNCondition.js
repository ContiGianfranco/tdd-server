'use strict';

const Component = require('./Component');

class NumericNCondition extends Component {
	verify() {
		let total = this.props.initial;
		for(let i = 0; i < this.props.children.length; i++)
			total = this.props.condition(total, this.props.children[i].verify());

		return total / this.props.div;
	}
}

module.exports = NumericNCondition;
