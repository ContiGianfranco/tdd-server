'use strict';

const Component = require('./Component');

class FirstCondition extends Component {

	verify() {
		return this.props.children[0].verify();
	}
}

module.exports = FirstCondition;
