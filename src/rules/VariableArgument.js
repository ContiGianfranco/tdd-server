'use strict';

const Component = require('./Component');
const Variables = require('../schemas/variables');

class VariableArgument extends Component {
	async verify(userInfo) {
		const val = await Variables.find({ userID: userInfo.userID, name: this.props.name });
		// eslint-disable-next-line prefer-destructuring
		console.log(val[0]);
		return val[0].value;
	}
}

module.exports = VariableArgument;
