'use strict';

const Component = require('./Component');
const Variables = require('../schemas/variables');

class SetVariableAction extends Component {
	async verify(userInfo) {
		const value = await this.props.children[0].verify(userInfo);

		const update = {
			$set: {
				userID: userInfo.userID,
				name: this.props.name,
				value
			}
		};
		const options = { upsert: true };
		Variables.updateOne({
			userID: userInfo.userID,
			name: this.props.name
		},
		update, options
		)
			.then(val => console.log(val));

		return (value);
	}
}

module.exports = SetVariableAction;
