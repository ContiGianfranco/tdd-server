'use strict';

const Component = require('./Component');

class NumericBinaryCondition extends Component {

	verify() {
		return this.props.condition(this.props.children[0].verify(), this.props.children[1].verify());
	}
}

module.exports = NumericBinaryCondition;
