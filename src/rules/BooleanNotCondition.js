'use strict';

const Component = require('./Component');

class BooleanNotCondition extends Component {

	verify() {
		return !(this.props.children[0].verify());
	}
}

module.exports = BooleanNotCondition;
