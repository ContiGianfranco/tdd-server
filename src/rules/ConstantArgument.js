'use strict';

const Component = require('./Component');


class ConstantArgument extends Component {
	verify() {
		return this.props.value;
	}
}

module.exports = ConstantArgument;
