'use strict';

class Component {
	constructor(props) {
		this.props = props;
	}

	verify() {
		return null;
	}

	add(child) {
		this.props.children.push(child);
	}

	remove(child) {
		this.props.children.slice(this.props.children.indexOf(child));
	}

	getChild(name) {
		return this.props.children.find(child => {
			return child.props.name === name;
		});
	}
}

module.exports = Component;
