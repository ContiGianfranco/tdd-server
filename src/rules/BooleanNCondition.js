'use strict';

const Component = require('./Component');

class BooleanNCondition extends Component {

	verify() {
		let tmp = this.props.children[0].verify();

		for(let i = 1; i < this.props.children.length; i++)
			tmp = this.props.condition(tmp, this.props.children[i].verify());

		return tmp;
	}
}

module.exports = BooleanNCondition;
