'use strict';

const UserInfo = require('../services/user-info');

class Rule {
	constructor(name, actions, condition, userID) {
		this.name = name;
		this.actions = actions;
		this.condition = condition;
		this.userID = userID;
	}

	async check() {
		const userInfo =	new UserInfo().getApiKeys(this.userID);
		if(this.condition.verify(userInfo)) {
			let temp = 0;
			for(const action of this.actions)
				temp = action.verify(userInfo);
			return temp;
		}
		return null;
	}
}

module.exports = Rule;
