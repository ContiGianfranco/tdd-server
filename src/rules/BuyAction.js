'use strict';

const CryptoJS = require('crypto-js');
const chai = require('chai');
const chaiHttp = require('chai-http');
const Transaction = require('../schemas/transaction');

chai.use(chaiHttp);

const Component = require('./Component');

const url = 'https://testnet.binance.vision/';
const route = 'api/v3/order';

class BuyAction extends Component {
	async verify(userInfo) {
		const now = new Date();
		const timestamp = now.getTime();
		const quantity = await this.props.children[0].verify(userInfo);

		const signature = CryptoJS.HmacSHA256(
			`symbol=${this.props.symbol}&type=MARKET&quantity=${quantity}&newOrderRespType=RESULT&timestamp=${timestamp}&side=BUY`,
			userInfo.privateKey
		).toString(CryptoJS.enc.Hex);

		const res = await chai.request(url)
			.post(route)
			.set({
				'Content-Type': 'application/json',
				'X-MBX-APIKEY': userInfo.publicKey
			})
			.query({
				symbol: this.props.symbol,
				type: 'MARKET',
				quantity,
				newOrderRespType: 'RESULT',
				timestamp,
				signature,
				side: 'BUY'
			});

		console.log('BUY_ACTION');
		let executeQty = res.body.executedQty;
		if(!res.body.executedQty)
			executeQty = res.body.msg;
		console.log({
			timestamp: now,
			symbol: this.props.symbol,
			executedQty: executeQty
		});

		const newTransaction = new Transaction({
			userID: userInfo.userID,
			date: now,
			symbol: this.props.symbol,
			executedQty: executeQty
		});
		await Transaction.create(newTransaction);
		return quantity;
	}
}

module.exports = BuyAction;
