'use strict';

const Component = require('./Component');

class ObjectNCondition extends Component {
	verify(userInfo) {
		return this.props.function.evaluate(this.props.children, userInfo);
	}
}

module.exports = ObjectNCondition;
