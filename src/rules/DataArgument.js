'use strict';

const Component = require('./Component');
const SymbolsSchema = require('../schemas/symbols');

class DataArgument extends Component {
	async verify() {
		const symbol = this.props.symbol.replace('/', '');
		const now = new Date();
		const from = new Date(now.getTime() - this.props.from * 1000);
		const until = new Date(now.getTime() - this.props.until * 1000);
		const values = await SymbolsSchema.find({
			symbol,
			'data.date': {
				$gte: from,
				$lte: until
			}
		},
		{
			_id: 0,
			'data.value': 1
		});
		// eslint-disable-next-line prefer-destructuring
		const temp = [];
		for(const val of values)
			temp.push(val.data.value);

		return temp;
	}
}

module.exports = DataArgument;
