'use strict';

const mongoose = require('mongoose');

const conditionsSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, 'Type of condition is required']
		},
		type: String,
		arguments: {
			type: Array,
			default: []
		}
	}
);

module.exports = mongoose.model('Conditions', conditionsSchema);
