'use strict';

const mongoose = require('mongoose');

const keysSchema = new mongoose.Schema(
	{
		userID: {
			type: String
			// required: [true, 'id is required']
		},
		privateKey: String,
		publicKey: String
	}
);


module.exports = mongoose.model('Keys', keysSchema);
