'use strict';

const mongoose = require('mongoose');

const amountSchema = new mongoose.Schema(
	{
		type: {
			type: String,
			required: [true, 'Type of amount is required']
		},
		value: Number,
		symbol: String
	}
);

const ArgumentsSchema = new mongoose.Schema(
	{
		type: String,
		value: { },
		name: String,
		symbol: String,
		arguments: [{}],
		from: Number,
		until: Number,
		default: [{}]
	}
);

const conditionsSchema = new mongoose.Schema(
	{
		name: String,
		type: {
			type: String,
			required: true
		},
		value: {},
		arguments: [ArgumentsSchema]
	}
);

const valuesSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: true
		},
		type: String,
		arguments: [ArgumentsSchema]
	}
);


const actionSchema = new mongoose.Schema(
	{
		type: {
			type: String,
			required: [true, 'Type of action is required']
		},
		name: String,
		symbol: String,
		amount: amountSchema,
		value: valuesSchema
	}
);

const rulesSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: true
		},
		condition: {
			type: conditionsSchema,
			required: [true, 'Condition is required']
		},
		action: {
			type: [actionSchema],
			required: [true, 'Action is required']
		},
		_userID: {
			type: String,
			required: [true, 'userID is required']
		}
	}
);

module.exports = mongoose.model('Rules', rulesSchema);
