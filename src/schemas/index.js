'use strict';

const userSchema = require('./user');
const fileSchema = require('./file');
const shareRequestSchema = require('./share-request');
const ruleSchema = require('./rules');
const transactionSchema = require('./transaction');

module.exports = {
	userSchema,
	fileSchema,
	shareRequestSchema,
	ruleSchema,
	transactionSchema
};
