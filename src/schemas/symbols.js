'use strict';

const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema(
	{
		date: Date,
		value: Number
	}
);

const symbolsSchema = new mongoose.Schema(
	{
		symbol: {
			type: String,
			required: [true, 'Type of symbol in symbol is required']
		},
		data: dataSchema
	}
);

module.exports = mongoose.model('Symbols', symbolsSchema);
