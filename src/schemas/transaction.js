'use strict';

const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema(
	{
		userID: String,
		date: Date,
		symbol: String,
		executedQty: { }
	}
);

module.exports = mongoose.model('Transaction', transactionSchema);
