'use strict';

const mongoose = require('mongoose');

const valuesSchema = new mongoose.Schema(
	{
		type: {
			type: String,
			required: [true, 'Type of value is required']
		},
		name: String,
		symbol: String,
		value: { },
		dateTime: Date,
		UserID: String
	}
);

module.exports = mongoose.model('Values', valuesSchema);
