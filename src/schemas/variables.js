'use strict';

const mongoose = require('mongoose');

const variablesSchema = new mongoose.Schema(
	{
		name: String,
		value: { },
		userID: String
	}
);


module.exports = mongoose.model('Variables', variablesSchema);
