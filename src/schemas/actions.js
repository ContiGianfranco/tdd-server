'use strict';

const mongoose = require('mongoose');

const amountSchema = new mongoose.Schema(
	{
		type: {
			type: String,
			required: [true, 'Type of amount is required']
		},
		value: { },
		symbol: String
	}
);

const actionSchema = new mongoose.Schema(
	{
		type: {
			type: String,
			required: [true, 'Type of action is required']
		},
		symbol: {
			type: String,
			required: [true, 'Type of symbol in action is required']
		},
		amount: {
			type: amountSchema
		}
	}
);

module.exports = mongoose.model('Actions', actionSchema);
