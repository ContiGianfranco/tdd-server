'use strict';

const Express = require('express');
const swaggerUi = require('swagger-ui-express');
const cors = require('cors');
const routes = require('./routes/index');
const SymbolHistory = require('./symbol-history/symbol-history');
const RulesManager = require('./rule-manager/rules-manager');
const UserInfo = require('./services/user-info');
require('./startup/startup-mongo')();

const options = require('../documentation/options');

const app = new Express();

app.use(cors({
	origin: '*'
}));


app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(options));

app.use('/api', routes);

const port = process.env.PORT || 8080;

new UserInfo();
new RulesManager();
new SymbolHistory();

app.listen(port, () => console.log(`App listening on port ${port}`));

module.exports = app;
