'use strict';

const { fileSchema, shareRequestSchema } = require('../schemas/index');
const Model = require('../databases/mongodb/model');
const getUserPath = require('../helpers/get-user-path');
const formatPath = require('../helpers/format-path');
const getUserDirectoryPath = require('../helpers/get-user-directory-path');
const validatePath = require('../helpers/validate-path');
const getPathWithoutUser = require('../helpers/get-path-without-user');
const validatePathDeleteFile = require('../helpers/validate-path-delete-file');
const users = require('./users');

const fileModel = new Model('Files', fileSchema);
const shareRequestModel = new Model('ShareRequests', shareRequestSchema);

const createFile = async ({ path, ...file }, user) => {

	const pathFormatted = getUserPath(validatePath(path), user);
	return fileModel.create({ ...file, path: pathFormatted, user });
};

const isShareFilePath = path => {

	const pathItems = validatePath(path).split('/');

	if(pathItems.length !== 2)
		return false;

	if(pathItems[0] === 'shared')
		return true;

	return false;

};

const getSharedFiles = async guest => {

	const { canRead, canShare } = await users.getUserById(guest);

	const [canReadFiles, canShareFiles] = await Promise.all([fileModel.find({ _id: canRead }), fileModel.find({ _id: canShare })]);

	const canReadFilesFormatted = canReadFiles.map(({ path, ...file }) => ({
		...file,
		canRead: true,
		canShare: false,
		path: '/shared/'
	}));

	const canShareFilesFormatted = canShareFiles.map(({ path, ...file }) => ({
		...file,
		canRead: true,
		canShare: true,
		path: '/shared/'
	}));

	return [...canReadFilesFormatted, ...canShareFilesFormatted];
};

const getFiles = async (path, user) => {

	if(isShareFilePath(path))
		return getSharedFiles(user);

	const userPath = getUserPath(validatePath(path), user);
	const files = await fileModel.findBy('path', userPath);
	return files.map(formatPath);
};

const getFile = async (path, user) => {

	const userFilePath = getUserPath(validatePath(path), user);

	const [userDirectoryPath, fileName] = getUserDirectoryPath(userFilePath);

	const files = await fileModel.find({ path: userDirectoryPath, name: fileName });

	const file = files.length ? formatPath(files[0]) : null;

	if(!file)
		return null;

	const sharedWith = await shareRequestModel.find({ fileId: file._id, accepted: true });

	if(!sharedWith.length)
		return { ...file, sharedWith: [] };

	const guestEmails = sharedWith.map(({ guestEmail }) => guestEmail);

	const usersItems = await users.getUsersByEmails(guestEmails);

	const usersItemsFormatted = usersItems.map(({ canShare, canRead, ...userToFormat }) => ({ ...userToFormat, canShare: canShare.includes(file._id) }));

	return { ...file, sharedWith: usersItemsFormatted };
};

const getSharedTree = async user => {

	const sharedFiles = await getSharedFiles(user);

	if(!sharedFiles.length)
		return null;

	return {
		id: 'shared',
		name: 'shared',
		path: '/shared/',
		children: sharedFiles.map(({ name, _id }) => ({
			id: _id,
			name,
			path: '/shared/',
			children: []
		}))
	};
};

const getTree = async user => {

	const files = await fileModel.find({ user });

	if(!files.length) {
		const sharedTree = await getSharedTree(user);
		return sharedTree ? [sharedTree] : [];
	}

	const paths = files.reduce((aux, file) => {
		const { path, name } = file;
		const pathFormatted = getPathWithoutUser(path);
		aux[pathFormatted.concat(name)] = { ...file, path: pathFormatted };
		return aux;
	}, {});

	const tree = [];
	const level = { result: tree };

	Object.entries(paths).forEach(([path, file]) => {
		path.split('/').reduce((r, name) => {
			if(!r[name]) {
				r[name] = { result: [] };
				r.result.push({
					id: file._id,
					name,
					children: r[name].result,
					path: file.type === 'file' ? '/'.concat(file.path) : '/'.concat(file.path.concat(file.name).concat('/'))
				});
			}
			return r[name];
		}, level);
	});

	const sharedTree = await getSharedTree(user);

	return sharedTree ? [{ id: 'root', name: '/', path: '/', children: tree }, sharedTree] :
		[{ id: 'root', name: '/', path: '/', children: tree }];
};

const getChildren = async (path, user) => {
	return getFiles(validatePath(path), user);
};

const deleteFile = async (path, user) => {

	const userPath = getUserPath(validatePathDeleteFile(path), user);
	console.log(userPath);
	const [userDirectoryPath, fileName] = getUserDirectoryPath(userPath);
	console.log(userDirectoryPath, fileName);
	return fileModel.remove({ path: userDirectoryPath, name: fileName });
};

const addShareRequest = request => {
	return shareRequestModel.create(request);
};

const acceptShareRequest = async _id => {
	await shareRequestModel.update({ _id }, { accepted: true });
	const [request] = await shareRequestModel.find({ _id });
	return request;
};

module.exports = {
	createFile,
	getFile,
	getFiles,
	getTree,
	getChildren,
	deleteFile,
	addShareRequest,
	acceptShareRequest
};
