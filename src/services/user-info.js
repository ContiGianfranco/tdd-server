'use strict';

const KeysSchema = require('../schemas/keys');

class UserInfo {

	constructor() {
		if(typeof UserInfo.instance === 'object')
			return UserInfo.instance;

		this.info = [];
		this.findApiKeys();
		UserInfo.instance = this;
		return this;


	}

	findApiKeys() {
		KeysSchema.find({}).then(val => {
			for(let i = 0; i < val.length; i++)
				this.info.push(val[i]);
		});

	}


	update() {
		this.info = [];
		this.findApiKeys();
	}

	getApiKeys(userID) {
		const actualUser = (this.info.find(user => user.userID === userID));
		// console.log(actualUser);
		return actualUser;
	}
}

module.exports = UserInfo;
