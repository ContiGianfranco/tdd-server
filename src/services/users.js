'use strict';

const Bcrypt = require('bcrypt');
const { userSchema } = require('../schemas/index');
const Model = require('../databases/mongodb/model');
const Mailer = require('../mailer/index');


const userModel = new Model('Users', userSchema);

const getEncriptedPassword = async password => {
	const salt = await Bcrypt.genSalt(10);
	return Bcrypt.hash(password, salt);
};

const createUser = async user => {

	if(user.provider === 'email') {
		const encriptedPassword = await getEncriptedPassword(user.password);
		return userModel.create({ ...user, password: encriptedPassword });
	}
	return userModel.create(user);
};

const validateCredentials = (passwordToValidate, password) => {
	return Bcrypt.compare(passwordToValidate, password);
};

const getUsersByEmails = email => {
	return userModel.find({ email });
};

const getUserById = async id => {
	const [user] = await userModel.findBy('_id', id);
	return user;
};

const getUserByEmail = async email => {
	const [user] = await userModel.findBy('email', email);
	return user;
};

const updateUser = async userUpdated => {

	let passwordToupdate = null;
	const { _id, newPassword, password, ...userFormatted } = userUpdated;

	if(newPassword)
		passwordToupdate = await getEncriptedPassword(newPassword);

	const updated = await userModel.update({ _id }, { ...userFormatted, ...passwordToupdate && { password: passwordToupdate } });

	if(!updated)
		throw new Error(`It was not possible to update user with ID: ${_id}`);

	return getUserById(_id);
};

const updateToken = async (_id, accessToken) => {
	return userModel.update({ _id }, { accessToken });
};

const removeToken = async _id => {
	return userModel.update({ _id }, { accessToken: null });
};

const updateGuest = async ({ guestEmail, fileId, canShare: requestCanShare }) => {

	const guest = await getUserByEmail(guestEmail);

	if(requestCanShare) {
		const { canShare } = guest;
		const canShareFilesUpdated = [...new Set([...canShare, fileId])];
		return userModel.update({ email: guestEmail }, { canShare: canShareFilesUpdated });
	}

	const { canRead } = guest;
	const canReadFilesUpdated = [...new Set([...canRead, fileId])];
	return userModel.update({ email: guestEmail }, { canRead: canReadFilesUpdated });
};

const sendEmail = (shareRequestId, guestEmail, hostEmail, fileName) => {

	const mailer = new Mailer();
	return mailer.sendInvitationEmail(shareRequestId, guestEmail, hostEmail, fileName);
};

const addNewRule = async (_id, currentUser, ruleToAdd) => {

	const rulesUpdated = [...currentUser.rules, ruleToAdd];

	const updated = await userModel.update({ _id }, { rules: rulesUpdated });

	if(!updated)
		throw new Error(`It was not possible to update user with ID: ${_id}`);
};

const removeRules = async _id => {


	const updated = await userModel.update({ _id }, { rules: [] });

	if(!updated)
		throw new Error(`It was not possible to update user with ID: ${_id}`);
};

const addNewKeys = async (currentUser, privateKey, publicKey) => {

	const updated = await userModel.update({ _id: currentUser._id }, { privateKey, publicKey });

	if(!updated)
		throw new Error(`It was not possible to add Keys to user with ID: ${currentUser._id}`);
};

const removeUser = async currentUser => {

	const { _id } = currentUser;

	const removed = await userModel.remove({ _id });

	if(!removed)
		throw new Error(`It was not possible to remove user with ID: ${_id}`);
};


module.exports = {
	createUser,
	updateUser,
	validateCredentials,
	getEncriptedPassword,
	getUserById,
	getUserByEmail,
	updateToken,
	removeToken,
	updateGuest,
	sendEmail,
	getUsersByEmails,
	addNewRule,
	removeRules,
	addNewKeys,
	removeUser
};
