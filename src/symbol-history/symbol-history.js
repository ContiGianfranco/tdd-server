'use strict';

const WebSocket = require('ws');
const RulesManager = require('../rule-manager/rules-manager');
const SymbolSchema = require('../schemas/symbols');

const DAYINMILSEC = 86400000;
const TWODAYSINMILSEC = 2 * DAYINMILSEC;

class SymbolHistory {

	constructor() {
		if(typeof SymbolHistory.instance === 'object')
			return SymbolHistory.instance;

		this.symbols = {
			BNBBTC: [],
			BNBBUSD: [],
			BNBUSDT: [],
			BTCBUSD: [],
			BTCUSDT: [],
			ETHBTC: [],
			ETHBUSD: [],
			ETHUSDT: [],
			LTCBNB: [],
			LTCBTC: [],
			LTCBUSD: [],
			LTCUSDT: [],
			TRXBNB: [],
			TRXBTC: [],
			TRXBUSD: [],
			TRXUSDT: [],
			XRPBNB: [],
			XRPBTC: [],
			XRPBUSD: [],
			XRPUSDT: []
		};

		for(const [symbol, info] of Object.entries(this.symbols)) {

			// eslint-disable-next-line no-console
			console.log(`wss://stream.binance.com:9443/ws/${symbol.toLowerCase()}@bookTicker`);

			this.socket = new WebSocket(`wss://stream.binance.com:9443/ws/${symbol.toLowerCase()}@bookTicker`);
			this.socket.addEventListener('message', async event => {
				try {
					if(info.length !== 0) {
						if(Math.abs((JSON.parse(event.data).b / info[info.length - 1].value) - 1) > 0.001) {
							new RulesManager().verify();
							const now = new Date();
							await SymbolSchema.deleteMany({ 'data.date': { $lt: new Date(now.getTime() - TWODAYSINMILSEC) } });

							const data = {
								date: new Date(),
								value: Number(JSON.parse(event.data).b)
							};
							const newSymbol = new SymbolSchema({ symbol, data });
							await SymbolSchema.create(newSymbol);
							console.log({
								symbol,
								date: new Date(),
								value: Number(JSON.parse(event.data).b)
							});

							info.push({
								date: new Date(),
								value: Number(JSON.parse(event.data).b)
							});
						}
					} else {
						// eslint-disable-next-line no-console
						console.log({
							symbol,
							date: new Date(),
							value: Number(JSON.parse(event.data).b)
						});
						info.push({
							date: new Date(),
							value: Number(JSON.parse(event.data).b)
						});
					}
				} catch(e) {
					// eslint-disable-next-line no-console
					console.log(e);
				}
			});
		}

		SymbolHistory.instance = this;
		return this;
	}

	getSymbolHistory(symbol, from, until) {
		const tmp = [];
		const now = new Date();

		for(const info of this.symbols[symbol]) {
			if((now - info.date) >= from * 1000 && (now - info.date) <= until * 1000)
				tmp.push(info.value);
		}
		return tmp;
	}
}

module.exports = SymbolHistory;
