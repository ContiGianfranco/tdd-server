'use strict';

const RulesSchema = require('../schemas/rules');
const FactoryRule = require('../Factory/FactoryRule');

class RulesManager {

	constructor() {
		if(typeof RulesManager.instance === 'object')
			return RulesManager.instance;

		const factoryRule = new FactoryRule();
		this.rules = [];
		this.findRules(factoryRule);

		RulesManager.instance = this;
		return this;
	}

	update() {
		this.rules = [];
		const factoryRule = new FactoryRule();
		this.findRules(factoryRule);
	}


	findRules(factoryRule) {
		RulesSchema.find({}, (err, rules) => {
			if(err)
				throw err;

			for(const rule of rules)
				this.rules.push(factoryRule.createRule(rule));
		});

	}

	verify() {
		for(const rule of this.rules)
			rule.check();

	}
}

module.exports = RulesManager;
