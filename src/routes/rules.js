'use strict';

const express = require('express');
const { rules } = require('../controllers/index');
const authenticateUser = require('../middleware/authenticate-user');
const { getVariables } = require('../controllers/rules');

const router = express.Router();

router.post('/variable', rules.addVariable);
router.post('/rule', rules.addRule);
router.delete('/rule', rules.removeRuleFromUserById);
router.get('/rule', rules.getAllRules);
router.post('/action', rules.addAction);
router.post('/condition', rules.addCondition);
router.post('/symbol', rules.addSymbol);
router.get('/transaction', rules.getTransaction);
router.get('/variable', getVariables);
router.post('/keys', authenticateUser, rules.addKeys);
router.get('/keys', rules.getKeys);

router.get('/', rules.getSymbolHistory);

module.exports = router;
