'use strict';

const express = require('express');
const { wallet } = require('../controllers/index');


const router = express.Router();

router.get('/', wallet.getWallet);

module.exports = router;
